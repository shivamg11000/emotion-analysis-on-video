const userVideo = document.getElementById('video');  // video element which shows captured webcam video of the user
const refreshRate = 100;
let detection;  // face detection
let intervalID = 0;  // only 1 interval will run at a time

// video element used to take webcam recording and face detection is applied on it
const inputVideo = document.createElement('video');  
inputVideo.width = video.width;
inputVideo.height = video.height;
inputVideo.autoplay = true;

function startVideo() {
  navigator.getWebcam = (navigator.getUserMedia || navigator.webKitGetUserMedia || navigator.moxGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia);
  if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
      navigator.mediaDevices.getUserMedia({ video: true })
      .then(loadStream)
      .catch(handleErr);
  }
  else {
    navigator.getWebcam(
      { video: {facingMode: 'user'},
        audio: false
      }, 
      loadStream, 
      handleErr
      );
  }

  function loadStream(stream){
    inputVideo.srcObject = stream
    userVideo.srcObject = stream
  }
  function handleErr(err) {
    console.log(err)
  }
}

// load all models
Promise.all([
  faceapi.nets.tinyFaceDetector.loadFromUri('/models'),
  faceapi.nets.faceLandmark68Net.loadFromUri('/models'),
  faceapi.nets.faceRecognitionNet.loadFromUri('/models'),
  faceapi.nets.faceExpressionNet.loadFromUri('/models')//,
  //faceapi.nets.ssdMobilenetv1.loadFromUri('/models')
])
.then(() => startVideo())
.then(() => userVideo.srcObject = inputVideo.srcObject) // show webcam video to user on first page
.catch((e)=>console.log(e))

// fisrt page

// as soon as video starts after loading of model
userVideo.addEventListener('play', () => {
  const canvas = createOverlay(userVideo); 
  intervalID = setInterval(async () => {
    // detect 
    detection = await detectFace();

    canvas.getContext('2d').clearRect(0, 0, canvas.width, canvas.height);
    if (!detection) return;        // if nothing detected                    
         
    showOverlay(detection, canvas);     
    console.log('here1');               
  }, refreshRate);
});

// overlay on video element in .first-page
function createOverlay(video){
  const canvas = faceapi.createCanvasFromMedia(video);
  canvas.classList.add('overlay');
  document.querySelector('.first-page').append(canvas);
  const displaySize = {width: video.width, height: video.height};
  faceapi.matchDimensions(canvas, displaySize);
  return canvas;
}
function showOverlay(detection, canvas){
  const displaySize = {width: canvas.width, height: canvas.height};
  const resizedDetections = faceapi.resizeResults(detection, displaySize);
  faceapi.draw.drawDetections(canvas, resizedDetections);
  faceapi.draw.drawFaceExpressions(canvas, resizedDetections);
}


// starting the funny video
const startBtn = document.getElementById("statVideo");

startBtn.addEventListener('click', function(){
  // remove first page & start funny video
  if (!detection) {  // if face not detected
    window.alert("Face not Detected");
    return;
  }
  document.querySelector('.first-page').classList.add('hide');
  document.querySelector('.second-page').classList.remove('hide');
  clearInterval(intervalID);   // stop detection
  intervalID = 0; 
});


// .second-page

const funVideo = document.getElementById('funny-video');
funVideo.src = "videos/Cartoon.mp4";
funVideo.load();

var data = [];   // emotion showed during whole of funny video

funVideo.addEventListener('play', function(e){   // funny video start playing
  if (intervalID!=0)  return; // if interval already running
  intervalID = setInterval(async () => {
    // detect 
    detection = await detectFace();

    if (!detection) return;        // if nothing detected                    
    const {emotion, probability} = getEmotion(detection);     // get emotion from detection       
    data.push({
      emotion: emotion,
      probability: probability,
      timeStamp:  getCurrentMinute()
    }); 
    console.log('here')
  }, refreshRate);
  //console.log(interval);
});

funVideo.addEventListener('ended', function(){   // funny video ended
  //console.log(interval);
  clearInterval(intervalID);
  // show analysis
  document.querySelector('.second-page').classList.add('hide');
  document.querySelector('.analysis-page').classList.remove('hide');
  const analysisData = doAnalysis(data);
  showAnalysis(analysisData);
});

/**    helper functions    **/

// get empotion from web cam face detection
function getEmotion(detection){
  let maxVal = -999;
  let maxValEmotion = 'neutral';
  delete detection.expressions.neutral;
  for (const [emotion, value] of Object.entries(detection.expressions)) {
    if (value>maxVal) {
      maxVal = value
      maxValEmotion = emotion
    }  
  }
  //console.log(maxValEmotion, maxVal);
  maxValEmotion = maxVal > 0.3 ? maxValEmotion : 'neutral';
  return {
    emotion: maxValEmotion,
    probability: maxVal
  };
}

function detectFace(){  // detect face expression, here promise is returned
  return faceapi.detectSingleFace(inputVideo, new faceapi.TinyFaceDetectorOptions())
                                .withFaceLandmarks()
                                .withFaceExpressions();
}

function doAnalysis(data){  // perform analysis on emotion data of a single video
  convertTimeStampToCurrentMinute(data);   
  //console.log(data);
  // get frequency of all emotions
  const freq = {};
  data.forEach(dataPoint => {
    const {emotion} = dataPoint;
    if(freq[emotion])
      freq[emotion]++;
    else
      freq[emotion] = 1;
  });
  normalizeFreq(freq);
  
  return {
    freq: freq,
    maxEmotion: getMaxEmotion(freq),
    EmotionsAndTime: getTimeOfEmotions(data)  // {emotion: time}, that time of emotion when it was max 
  };
}

const colors = ["#2ecc71", "#3498db", "#9b59b6", "#f1c40f", "#e74c3c", "#34495e"];

function showAnalysis(data){   // show the analysed data
  
  const {freq, maxEmotion, EmotionsAndTime} = data;
  // draw pie
  const ctx = document.getElementById('myChart').getContext('2d');
  const chartData = {
    datasets: [{
        data: Object.values(freq),
        backgroundColor: colors 
    }],
    // These labels appear in the legend and in the tooltips when hovering different arcs
    labels: Object.keys(freq) 
  };
  const options = {
    title: {
      display: true,
      text: 'Emotions showed during the video'
    }
  };
  var myPieChart = new Chart(ctx, {
    type: 'pie',
    data: chartData,
    options: options
  });

  // show summary
  //console.log(maxEmotion, EmotionsAndTime);
  
  const html = Object.keys(EmotionsAndTime).map(emotion => {
    const time = EmotionsAndTime[emotion];
    return (
      `<div class="px-4 py-2 my-4 item">
        <p class="m-0">User was most ${emotion} at ${convertSec(time)} second</p>
       </div>`
    );
  });

  document.querySelector('.summary').innerHTML = 
  `<div class="px-4 py-2 my-4 item">
    <p class="m-0">User was ${maxEmotion} during video</p>
   </div>` + html.join("");
    
}

function normalizeFreq(freq){  // calcs percentage on freq
  const total = Object.values(freq).reduce((sum, val) => sum+val,0);

  Object.keys(freq).forEach((key) => {
    freq[key] = Math.round((freq[key]/total) * 100);
  });
}
function getMaxEmotion(freq){ // return max emotion in freq
  let _freq = Object.assign({}, freq);  // clone
  delete _freq.neutral;
  return Object.keys(_freq).reduce((a, b) => _freq[a] > _freq[b] ? a : b, -1);
}
function getCurrentMinute(){  // since 1 Jan 1970
  return Math.round(Date.now() / (1000));
}
function convertTimeStampToCurrentMinute(data){  // convert the timestamp of each data into minute with respect to the start of video
  const startTimeStamp = data[0].timeStamp;

  data.forEach(obj => obj.time = obj.timeStamp - startTimeStamp);
}
function getTimeOfEmotions(data){  // return that time for which a particular emotion was max
  let res = {};  // stores that time for a emotion for which that emotion prob. was max
  let tmp = {}; // stores max prob of a particular emotion, {emotion, prob} 
  data.forEach(obj => {
    const {emotion, probability, time} = obj;
    if (res[emotion]){
      tmp[emotion] = tmp[emotion] < probability ? probability : tmp[emotion];
      res[emotion] = tmp[emotion] < probability ? time : res[emotion];
    }
    else{
      tmp[emotion] = probability;
      res[emotion] = time;
    }
  })
  delete res.neutral;
  return res;
}
function convertSec(sec){  // 2 sec -> 0:02, 61 sec -> 01:01
  _sec = Math.floor(sec);
  const minutes = Math.floor(_sec/60);
  const seconds = _sec % 60;
  let str = '';
  if(minutes<10)
    str += `0${minutes}:`;
  else 
    str += `${minutes}:`;
  if(seconds<10)
    str += `0${seconds}`;
  else 
    str += `${seconds}`;
  return str;        
}